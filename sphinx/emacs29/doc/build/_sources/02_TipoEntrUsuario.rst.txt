.. _2:

2 Tipos de Entrada del Usuario
==============================

GNU Emacs está diseñado principalmente para usarse con el teclado. Aunque es posible usar el ratón para emitir comandos de edición a través de la barra de menús y la barra de herramientas, normalmente no es tan eficiente como usar el teclado.

La entrada del teclado en Emacs se basa en una versión muy extendida de ASCII. Los caracteres simples, como ``a``, ``B``, ``3``, ``=``, y el carácter de espacio (denotado como ``SPC``), se introducen tecleando la tecla correspondiente. Los *caracteres de control*, como ``RET``, ``TAB``, ``DEL``, ``ESC``, ``F1``, ``Inicio`` e ``IZQUIERDA``, también se introducen de esta forma, al igual que algunos caracteres que se encuentran en teclados no ingleses (véase :ref:`23`).

Emacs también reconoce los caracteres de control que se introducen usando *teclas modificadoras*. Dos teclas modificadoras de uso común son ``Control`` (:kbd:`Ctrl` normalmente denominada ``Ctrl``) y ``Meta`` (:kbd:`Alt` normalmente denominada ``Alt``) [3]_. Por ejemplo, ``Control-a`` se introduce manteniendo pulsada la tecla :kbd:`Ctrl` (``Ctrl``) mientras se pulsa :kbd:`a` (``a``); nos referiremos a esto como :kbd:`Ctrl`-:kbd:`a` (``C-a``) para abreviar. Del mismo modo, ``Meta-a``, o ``M-a`` para abreviar, se introduce manteniendo pulsada la tecla :kbd:`Alt` (``Alt``) y pulsando :kbd:`a` (``a``). Las teclas modificadoras también pueden aplicarse a caracteres no alfanuméricos, por ejemplo, :kbd:`Ctrl`-:kbd:`F1` (``C-F1``) o :kbd:`Alt`-:kbd:`←` (``M-LEFT``).

También puede escribir caracteres Meta usando secuencias de dos caracteres empezando por ``ESC``. Así, puede introducir ``M-a`` escribiendo :kbd:`ESC`-:kbd:`a`. Puede introducir :kbd:`Ctrl`-:kbd:`Alt`-:kbd:`a` (``C-M-a``) (manteniendo pulsadas las teclas :kbd:`Ctrl` (``Ctrl``) y :kbd:`Alt` (``Alt``), y pulsando :kbd:`a` (``a``)) escribiendo :kbd:`ESC` :kbd:`Ctrl`-:kbd:`a` (``ESC C-a``). A diferencia de ``Meta``, ``ESC`` se introduce como un carácter independiente. No debe mantener pulsado :kbd:`ESC` (``ESC``) mientras escribe el siguiente carácter; en su lugar, pulse :kbd:`ESC` (``ESC``) y suéltelo, después introduzca el siguiente carácter. Esta función es útil en algunos terminales de texto en los que la tecla :kbd:`Alt` (``Meta``) no funciona de forma fiable.

Emacs soporta 3 teclas modificadoras adicionales, ver :ref:`50.3.7`.

Emacs tiene un amplio soporte para el uso de botones y rueda de ratón, y otros dispositivos señaladores como touchpads y pantallas táctiles. Ver :ref:`4` para más detalles.

En ciertos entornos, el gestor de ventanas puede bloquear algunas entradas de teclado, incluyendo :kbd:`Alt`-:kbd:`TAB` (``M-TAB``), :kbd:`Alt`-:kbd:`SPACE` (``M-SPC``), :kbd:`Ctrl`-:kbd:`Alt`-:kbd:`d` (``C-M-d``) y :kbd:`Ctrl`-:kbd:`Alt`-:kbd:`l` (``C-M-l``). Si tiene este problema, puede personalizar su gestor de ventanas para que no bloquee esas teclas, o volver a enlazar los comandos Emacs afectados (vea :ref:`50`).

Los caracteres simples y los caracteres de control, así como ciertas entradas que no son de teclado, como los clics del ratón, se denominan colectivamente eventos de entrada. Para más detalles sobre cómo Emacs maneja internamente los eventos de entrada, **vea Eventos de Entrada** en el Manual de Referencia de Emacs Lisp.

.. [3] Nos refermios a ``Alt`` (:kbd:`Alt`) como ``Meta`` por razones históricas.
