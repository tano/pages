.. _11:

11 Ayuda
========

Emacs proporciona una amplia variedad de comandos de ayuda, todos accesibles a través de la tecla prefijo  :kbd:`Ctrl`-:kbd:`h`  (``C-h`` o, equivalentemente, la tecla de función
:kbd:`F1` (``F1``)). Estos comandos de ayuda se describen en las siguientes secciones. También puede escribir :kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`h` (``C-h C-h``) para ver una lista de comandos de ayuda (``help-for-help``). Puede desplazarse por la lista con  :kbd:`SPACE`  (``SPC``) y :kbd:`DEL` (``DEL``) y, a continuación, escribir el comando de ayuda que desee. Para cancelar, escriba  :kbd:`Ctrl`-:kbd:`g` (``C-g``).

Muchos comandos de ayuda muestran su información en un búfer especial, el *búfer de ayuda*. En este búfer, puede escribir :kbd:`SPACE` (``SPC``) y :kbd:`DEL` (``DEL``) para desplazarse y :kbd:`RETURN`  (``RET``) para seguir los hipervínculos. Consulte :ref:`11.5`.

Por defecto, los comandos de ayuda muestran el búfer de ayuda en una ventana separada sin seleccionar dicha ventana. La variable ``help-window-select`` controla esto: su valor por defecto es ``nil``; si se personaliza con el valor ``t``, la ventana de ayuda es seleccionada incondicionalmente por los comandos de ayuda, y si su valor es ``other``, la ventana de ayuda es seleccionada sólo si hay más de dos ventanas en el marco seleccionado.

Por el contrario, muchos comandos del búfer ``*Help*`` (Ayuda) abrirán una nueva ventana para mostrar los resultados. Por ejemplo, hacer clic en el enlace para mostrar el código fuente, o usar el comando ``i`` para mostrar la entrada del manual, abrirá (por defecto) una nueva ventana. Si ``help-window-keep-selected`` se cambia a no-``nil``, la ventana que muestra el búfer ``*Help*`` se reutilizará en su lugar.

Si está buscando una determinada función, pero no sabe cómo se llama o dónde buscarla, le recomendamos tres métodos. Primero, pruebe un comando apropos, luego intente buscar en el índice  del manual, después busque en las FAQ y en las palabras clave del paquete, y finalmente intente listar paquetes externos.

:kbd:`Ctrl`-:kbd:`h` :kbd:`a` *argumento* :kbd:`RETURN` (``C-h a`` *argumento* ``RET``)
     
     Busca comandos cuyos nombres coincidan con los temas del *argumento*. El argumento puede ser una palabra clave, una lista de palabras clave o una expresión regular (véase :ref:`16.6`). Véase :ref:`11.4`.

| :kbd:`Ctrl`-:kbd:`h` :kbd:`i`:kbd:`d`:kbd:`m` *emacs* :kbd:`RETURN` :kbd:`i` *argumento* :kbd:`RETURN`
| (``C-h i d m`` ``emacs`` ``RET`` ``i`` *argumento* ``RET``)

    Busca en los índices del manual Emacs Info el *tema* especificado como *argumento*, mostrando la primera coincidencia encontrada. Pulse ``,`` para ver las siguientes coincidencias.
    Puede usar una expresión regular como *argumento*.

| :kbd:`Ctrl`-:kbd:`h` :kbd:`i`:kbd:`d`:kbd:`m` *emacs* :kbd:`RETURN` :kbd:`s` *argumento* :kbd:`RETURN`
| (``C-h i d m`` ``emacs`` ``RET`` ``s`` *argumento* ``RET``)

    Similar, pero busca en el texto del manual en lugar de en los índices.

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`f` (``C-h C-f``)

     Muestra las FAQ (Preguntas Frecuentes) de Emacs, usando Info.

:kbd:`Ctrl`-:kbd:`h` :kbd:`p` (``C-h p``)

     Muestra los paquetes Emacs disponibles basándose en palabras clave. Ver :ref:`11.6`.

:kbd:`Alt`-:kbd:`x` ``list-packages`` (``M-x list-packages``)

     Muestra una lista de paquetes externos. **Ver Paquetes Emacs Lisp**.

:kbd:`Ctrl`-:kbd:`h` (``C-h``) o :kbd:`F1` (``F1``) también significan "ayuda" en otros contextos. Por ejemplo, puede escribirlos después de una tecla de prefijo para ver una lista de las teclas que pueden seguir a la tecla de prefijo. (También puede usar ``?`` en este contexto. Algunas teclas de prefijo no admiten ``C-h`` o ``?`` de este modo, porque definen otros significados para esas entradas, pero todas admiten ``F1``).

11.1 Resumen de la Ayuda
------------------------

A continuación se ofrece un resumen de los comandos de ayuda para acceder a la documentación incorporada. La mayoría de ellos se describen con más detalle en las secciones siguientes.

:kbd:`Ctrl`-:kbd:`h` :kbd:`a` *argumento* :kbd:`RETURN` (``C-h a`` *argumento* ``RET``)

     Muestra una lista de comandos cuyos nombres coinciden con *argumento* (``apropos-command``). Véase :ref:`11.4`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`b` (``C-h b``)

     Muestra todas las combinaciones de teclas activas; primero las del modo menor, luego las del modo mayor y, por último, las combinaciones globales (``describe-bindings``). Consulte :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`c` *tecla* (``C-h c`` *tecla*)

     Muestra el nombre del comando al que está vinculada la tecla de secuencia de *teclas* (``describe-key-briefly``, describe-tecla-brevemente). Aquí ``c`` significa "carácter". Para obtener información más amplia sobre la *tecla*, use :kbd:`Ctrl`-:kbd:`h` :kbd:`k` (``C-h k``). Consulte :ref:`11.2`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`d` *argumento* :kbd:`RETURN` (``C-h d`` *argumento* ``RET``)

     Muestra los comandos y variables cuya documentación coincide con *argumento* (``apropos-documentation``). Véase :ref:`11.4`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`e` (``C-h e``)

     Muestra el búfer ``*Messages*`` (``view-echo-area-messages``). Consulte :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`f` *función* :kbd:`RETURN` (``C-h f`` *función* ``RET``)

     Muestra documentación sobre la función Lisp de nombre *función* (``describe-function``). Dado que los comandos son funciones Lisp, esto también funciona para comandos, pero también puede usar ``C-h x``. Consulte :ref:`11.3`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`h` (``C-h h``)

     Muestra el archivo ``HELLO``, que contiene ejemplos de varios juegos de caracteres.

:kbd:`Ctrl`-:kbd:`h` :kbd:`i` (``C-h i``)

     Ejecuta Info, el navegador de documentación de GNU (``info``). El manual de Emacs está disponible en Info. Ver :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`k` *tecla* (``C-h k`` *tecla*)

     Muestra el nombre y la documentación del comando que ejecuta *teclas* (``describe-key``). Véase :ref:`11.2`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`l` (``C-h l``)

     Muestra una descripción de las últimas 300 pulsaciones de teclas (``view-lossage``). Consulte :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`m` (``C-h m``)

     Muestra la documentación del modo mayor y los modos menores actuales (``describe-mode``). Consulte :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`n` (``C-h n``)

     Muestra noticias de cambios recientes en Emacs (^view-emacs-news``). Ver :ref:`11.9`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`o` *simbolo* (``C-h o`` *símbolo*)

     Muestra la documentación del símbolo Lisp de nombre *simbolo* (``describe-symbol``). Esto mostrará la documentación de todo tipo de símbolos: funciones, variables y caras. Véase :ref:`11.3`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`p` (``C-h p``)

     Busca paquetes usando palabra clave como temática ( finder-by-keyword). Véase :ref:`11.6`. Lista paquetes usando un búfer de menú de paquetes. Ver :ref:`49`.

:kbd:`Ctrl`-:kbd:`P` *paquete* (``C-h P`` *paquete* ``RET``)

     Muestra documentación sobre el *paquete* especificado (``describe-package``). Véase :ref:`49`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`r` (``C-h r``)

     Muestra el manual de Emacs en Info (``info-emacs-manual``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`s` (``C-h s``)

     Muestra el contenido de la *tabla de sintaxis* actual ``(describe-syntax``). Consulte :ref:`11.8`. La tabla de sintaxis dice qué caracteres son delimitadores de apertura, cuáles son partes de palabras, etc. **Vea Tablas de Sintaxis** en El Manual de Referencia de Emacs Lisp, para más detalles.

:kbd:`Ctrl`-:kbd:`h` :kbd:`t` (``C-h t``)

     Accede al tutorial interactivo de Emacs (``help-with-tutorial``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`v` *var* :kbd:`RETURN` (``C-h v`` *var* ``RET``)

     Muestra la documentación de la variable Lisp de nombre *var* (``describe-variable``). Véase :ref:`11.3`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`w` *comando* :kbd:`RETURN` (``C-h w`` *comando* ``RET``)

     Muestra qué tecla/s ejecuta/n la orden de nombre *comando* (``where-is``). Véase :ref:`11.2`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`x` *comando* :Kbd:`RETURN` (``C-h x`` *comando* ``RET``)

     Muestra documentación sobre el *comando* escrito (``describe-command``). Véase :ref:`11.3`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`C` *codificación* (``C-h C`` *codificación* ``RET``)

     Permite mostrar el sistema de *codificación* (``describe-coding-system``). Véase :ref:`23.5`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`C` :kbd:`RETURN` (``C-h C RET``)

     Muestra los sistemas de codificación usados actualmente.

:kbd:`Ctrl`-:kbd:`h` :kbd:`F` *comando* :kbd:`RETURN` (``C-h F`` *comando* ``RET``)

     Entra en Info y va al nodo que documenta el *comando* de Emacs escrito (``Info-goto-emacs-command-node``). Véase :ref:`11.3`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`I` *método* :kbd:`RETURN` (``C-h I`` *método* ``RET``)

     Muestra el método de *entrada* escrito (``describe-input-method``). Véase :ref:`23.4`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`K` *tecla* (``C-h K`` *tecla*)

xsy     Entra en Info y va al nodo que documenta la combinación de *teclas* (``Info-goto-emacs-key-command-node``). Véase :ref:`11.2`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`L` *entorno-de-idioma* :kbd:`RETURN` (``C-h L`` *entorno-de-idioma* ``RET``)

     Muestra información sobre los conjuntos de caracteres, sistemas de codificación y métodos de entrada escritos en *entorno-de-idioma* (``describe-language-environment``). Véase :ref:`23.2`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`S` *símbolo* :kbd:`RETURN` (``C-h S`` *símbolo* ``RET``)

     Muestra la documentación Info sobre *símbolo* según el lenguaje de programación que esté editando (``info-lookup-symbol``). Consulte :ref:`11.8`.

:kbd:`Ctrl`-:kbd:`h` :kbd:`.` (``C-h .``)

     Muestra el mensaje de ayuda para un área de texto especial, si el punto está en una (``display-local-help``). (Esto incluye, por ejemplo, enlaces en los búferes ``*Help*``.) Ver :ref:`11.10`. Si invoca este comando con un argumento prefijo, :kbd:`Ctrl`-:kbd:`u` :kbd:`Ctrl`-:kbd:`h` :kbd:`.` (``C-u`` ``C-h .``), y el punto está sobre un botón o un widget, este comando abrirá un nuevo búfer que describe ese botón/widget.

.. _11.2:

11.2 Documentación de una Tecla
-------------------------------

Los comandos de ayuda para obtener información sobre una secuencia de teclas son :kbd:`Ctrl`-:kbd:`h` :kbd:`c`   (``C-h c``, ``describe-key-briefly``, describe-tecla-brevemente) y :kbd:`Ctrl`-:kbd:`h` :kbd:`k` ( ``C-h k``, ``describe-key``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`c` *tecla* (``C-h c`` *tecla*) muestra en el área de eco el nombre del comando al que está vinculada esa *tecla*. Por ejemplo, :kbd:`Ctrl`-:kbd:`h` :kbd:`c` :kbd:`Ctrl`-:kbd:`f` (``C-h c C-f``) muestra ``forward-char``.

La combinación :kbd:`Ctrl`-:kbd:`h` :kbd:`k` *tecla* (``C-h k`` *tecla*) es similar pero da más información: muestra un búfer de ayuda que contiene la cadena de documentación del comando, que describe exactamente lo que hace ese comando.

:kbd:`Ctrl`-:kbd:`h` :kbd:`K` *tecla* (``C-h K`` *tecla*) muestra la sección del manual de Emacs que describe el comando correspondiente a *tecla*.

``C-h c``, ``C-h k`` y ``C-h K`` funcionan para cualquier tipo de secuencias de teclas, incluidas las teclas de función, los menús y los eventos del ratón (excepto que ``C-h c`` ignora los eventos de movimiento del ratón). Por ejemplo, después de :kbd:`Ctrl`-:kbd:`h` :kbd:`k` (``C-h k``) puede seleccionar un elemento de menú de la barra de menús, para ver la cadena de documentación del comando que ejecuta.

:kbd:`Ctrl`-:kbd:`h` :kbd:`w` *comando* :kbd:`RETURN` (``C-h w`` *comando* ``RET``) lista las teclas que están vinculadas al *comando*. Muestra la lista en el área de eco. Si dice que  *comando* no está en ninguna tecla, significa que debe usar :kbd:`Alt`-:kbd:`x` (``M-x``) para ejecutarlo. :kbd:`Ctrl`-:kbd:`h` :kbd:`w` (``C-h w``) ejecuta el *comando* donde está.

Algunos modos en Emacs usan varios botones (**ver Botones** en El Manual de Referencia de Emacs Lisp) y widgets (**ver Introducción** en Emacs Widgets) que pueden ser pulsados para realizar alguna acción. Para averiguar qué función es invocada en última instancia por estos botones, Emacs proporciona los comandos ``button-describe`` y ``widget-describe``, que deben ejecutarse con point sobre el botón.

.. _11.3:

11.3 Ayuda por Comando o Nombre de Variable
-------------------------------------------

:kbd:`Ctrl`-:kbd:`h` :kbd:`x` *comando* :kbd:`RETURN` (``C-h x`` *comando* ``RET``, ``describe-command``) muestra la documentación del *comando* citado en una ventana. Por ejemplo,

.. code-block::

   C-h x auto-fill-mode RET

muestra la documentación de ``auto-fill-mode``. Así es como obtendría la documentación de un comando que no está ligado a ninguna tecla (uno que normalmente ejecutaría usando ``M-x``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`f` *función* :kbd:`RETURN` (``C-h f`` *función* ``RET``, ``describe-function``) muestra la documentación de una *función* Lisp. Este comando está pensado para *funciones* Lisp que se utilizan en un programa Lisp. Por ejemplo, si acaba de escribir la expresión (``make-vector len``) y desea comprobar que está usando ``make-vector`` correctamente, escriba :kbd:`Ctrl`-:kbd:`h` ``make-vector`` :kbd:`RETURN`  (``C-h f make-vector RET``). Además, dado que todos los comandos son funciones Lisp, también puede utilizar este comando para ver la documentación de cualquier comando.

Si escribe :kbd:`Ctrl`-:kbd:`h` :kbd:`f` :kbd:`RETURN` (``C-h f RET``), se describe la función llamada por la expresión Lisp más interna en el búfer alrededor del punto, *siempre que* el nombre de la función sea una función Lisp válida y definida. (Ese nombre aparece por defecto mientras se introduce el argumento.) Por ejemplo, si el punto se encuentra a continuación del texto ``(make-vector (car x)``, la lista más interna que contiene punto es la que empieza por ``(make-vector``, por lo que ``C-h f RET`` describe la función ``make-vector``.

``C-h f`` también es útil para verificar que ha escrito correctamente el nombre de una función. Si el prompt del minibúfer para ``C-h f`` muestra el nombre de la función del búfer por defecto, significa que ese nombre está definido como una función Lisp. Escriba :kbd:`Ctrl`-:kbd:`g`  (``C-g``) para cancelar el comando ``C-h f`` si realmente no desea ver la documentación.

Si solicita ayuda para una función autocargada cuyo formulario ``autoload`` (**vea Autocarga** en el Manual de Referencia de Emacs Lisp) no proporciona una cadena doc, el búfer ``*Help*`` no tendrá ninguna cadena doc para mostrar. En ese caso, si ``help-enable-symbol-autoload`` es distinto de ``nil``, Emacs intentará cargar el fichero en el que está definida la función para ver si hay una cadena doc allí.

Puede obtener una visión general de las funciones relevantes para un tema en particular usando el comando :kbd:`Alt`-:kbd:`x` ``shortdoc`` (``M-x shortdoc``). Esto le preguntará por un área de interés, por ejemplo, :kbd:`string` (cadena), y le llevará a un búfer donde se listan muchas de las funciones relevantes para el manejo de cadenas.

``C-h v`` (``describe-variable``) es como ``C-h f`` pero describe variables Lisp en lugar de funciones Lisp. Su valor por defecto es el símbolo Lisp alrededor o antes de punto, si ese es el nombre de una variable Lisp definida. Véase :ref:`50.2`.

Los búferes de ayuda que describen variables y funciones Emacs normalmente tienen hipervínculos al código fuente correspondiente, si tiene instalados los archivos fuente (vea :ref:`47`).

Para encontrar la documentación de un comando en un manual, use :kbd:`Ctrl`-:kbd:`h` :kbd:`F` (``C-h F``, ``Info-goto-emacs-command-node``). Esto conoce varios manuales, no sólo el manual de Emacs, y encuentra el correcto.

``C-h o`` (``describe-symbol``) es como ``C-h f`` y ``C-h v``, pero describe cualquier símbolo, ya sea una función, una variable o una cara. si el símbolo tiene más de una definición, como por ejemplo tiene ambas definiciones, como función y como variable, este comando mostrará la documentación de todas ellas, una tras otra.

Si la opción de Usuario ``completions-detailed`` no es nula, algunos comandos proporcionan detalles sobre los posibles valores al mostrar las terminaciones. Por ejemplo, :kbd:`Ctrl`-:kbd:`h` (``C-h``) o :kbd:`TAB` (``TAB``) incluirá la primera línea de la cadena doc, y también dirá si cada símbolo es una función o una variable (y así sucesivamente). Los detalles que se incluyen varían en función del comando usado 

.. _11.4:

11.4 Apropos
------------

Los comandos *apropos* responden a preguntas como: "¿Cuáles son los comandos para trabajar con archivos?". Más precisamente, Usted especifica su consulta como un *patrón apropos*, que puede ser una palabra, una lista de palabras o una expresión regular.

Cada uno de los siguientes comandos apropos lee un patrón apropos en el minibúfer, busca elementos que coincidan con el patrón y muestra los resultados en una ventana diferente.

:kbd:`Ctrl`-:kbd:`h` :kbd:`a` (``C-h a``)

      Busca comandos (``apropos-command``). Con un argumento prefijo, busca también funciones no interactivas.

:kbd:`Alt`-:kbd:`x` ``apropos`` (``M-x apropos``)

     Busca funciones y variables. Se pueden encontrar tanto funciones interactivas (comandos) como funciones no interactivas.

:kbd:`Alt`-:kbd:`x` ``apropos-user-options`` (``M-x apropos-user-options``)

     Busca variables personalizables por el Usuario. Con un argumento prefijo, busca también variables no personalizables.

:kbd:`Alt`-:kbd:`x` ``apropos-variable`` (``M-x apropos-variable``)

     Busca de variables. Con un argumento prefijo, busca sólo variables personalizables.

:kbd:`Alt`-:kbd:`x` ``apropos-local-variable`` (``M-x apropos-local-variable``)

     Busca de variables locales en el búfer.

:kbd:`Alt`-:kbd:`x` ``apropos-value`` (``M-x apropos-value``)

     Busca variables cuyos valores coincidan con el patrón especificado. Con un argumento prefijo, busca también funciones con definiciones que coincidan con el patrón, y símbolos Lisp
     con propiedades que coincidan con el patrón.

:kbd:`Alt`-:kbd:`x` ``apropos-local-value`` (``M-x apropos-local-value``)

     Busca variables locales del búfer cuyos valores coincidan con el patrón especificado.

:kbd:`Ctrl`-:kbd:`h` :kbd:`d` (``C-h d``)

     Busca de funciones y variables cuyas cadenas de documentación coincidan con el patrón especificado (``apropos-documentation``).

El tipo más simple de patrón apropos es una palabra. Cualquier cosa que contenga esa palabra coincide con el patrón. Así, para encontrar comandos que funcionen con archivos, escriba   :kbd:`Ctrl`-:kbd:`h` :kbd:`a` *archivo* :kbd:`RETURN` (``C-h a`` *archivo* ``RET``). Esto muestra una lista de todos los nombres de comandos que contienen *archivo*, incluyendo ``copy-file`` (copiar-archivo), ``find-file`` (buscar-archivo), etc. Cada nombre de comando viene con una breve descripción y una lista de comandos. Cada nombre de comando viene con una breve descripción y una lista de teclas con las que puede invocarlo actualmente. En nuestro ejemplo, diría que puede invocar ``find-dile`` tecleando :kbd:`Ctrl`-:kbd:`x` :kbd:`Ctrl`-:kbd:`f` (``C-x C-f``).

Por defecto, la ventana que muestra el búfer apropos con los resultados de la consulta no está seleccionada, pero puede hacer que lo esté personalizando la variable ``help-window-select`` con cualquier valor que no sea nulo.

Para obtener más información sobre una definición de función, variable o propiedad de símbolo que aparece en un búfer apropos, puede hacer clic en él con ``ratón-1`` o ``ratón-2``, o desplazarse hasta allí y escribir :kbd:`RETURN` (``RET``).

Cuando se especifica más de una palabra en el patrón apropos, un nombre debe contener al menos dos de las palabras para coincidir. Así, si está buscando comandos para matar un trozo de texto antes de punto, podría intentar ``C-h a`` ``kill backward behind before`` ``RET``. El nombre real del comando ``kill-backward`` coincidirá con eso; si hubiera un comando ``kill-text-before``, también coincidiría, ya que contiene dos de las palabras especificadas.

Para una flexibilidad aún mayor, puede especificar una expresión regular (consulte :ref:`16.6`). Un patrón apropos se interpreta como una expresión regular si contiene alguno de los caracteres especiales de expresión regular, ``^$*+?.\[``.

Siguiendo las convenciones para nombrar los comandos de Emacs, aquí hay algunas palabras que encontrará útiles en los patrones apropos. Usándolas en :kbd:`Ctrl`-:kbd:`h` :kbd:`a` (``C-h a``), también se hará una idea de las convenciones de nomenclatura.

    char, line, word, sentence, paragraph, region, page, sexp, list, defun, rect, buffer, frame, window, face, file, dir, register, mode, beginning, end, forward, backward, next,
    previous, up, down, search, goto, kill, delete, mark, insert, yank, fill, indent, case, change, set, what, list, find, view, describe, default.

    caracter, linea, palabra, frase, parrafo, region, pagina, expresión, lista, definición de función, rect, búfer, marco, ventana, cara, archivo, dir, registro, modo, principio, fin,
    adelante, atras, siguiente, anterior, arriba, abajo, buscar, ir a, matar, borrar, marcar, insertar, tirar, rellenar, sangrar, caso, cambiar, establecer, que, lista, encontrar, ver,
    describir, por defecto.

Si la variable ``apropos-do-all`` no es nula, la mayoría de los comandos apropos se comportan como si se les hubiera dado un argumento prefijo. Hay una excepción: ``apropos-variable`` sin un argumento prefijo siempre buscará todas las variables, sin importar cuál sea el valor de ``apropos-do-all``.

Por defecto, todos los comandos apropos excepto ``apropos-documentation`` listan sus resultados en orden alfabético. Si la variable ``apropos-sort-by-scores`` no es nula, estos comandos intentan adivinar la relevancia de cada resultado y muestran primero los más relevantes. El comando ``apropos-documentation`` lista sus resultados en orden de relevancia por defecto; para listarlos en orden alfabético, cambie la variable ``apropos-documentation-sort-by-scores`` a ``nil``.

.. _11.5:

11.5 Comandos del Modo Ayuda
----------------------------

Los búferes de ayuda tienen el modo Ayuda como modo principal. El modo Ayuda proporciona los mismos comandos que el modo Vista (ver :ref:`15.6`); por ejemplo, :kbd:`SPACE` (``SPC``) se desplaza hacia adelante, y ``DEL`` o :kbd:`Shift`-:kbd:`SPACE` (``S-SPC``) se desplaza hacia atrás. También proporciona algunos comandos especiales:

:kbd:`RETURN` (``RET``)

     Sigue una referencia cruzada en el punto (``help-follow``).

:kbd:`TAB` (``TAB``)

     Hacer avanzar el punto hasta el siguiente hipervínculo (``forward-button``).

:kbd:`Shift`-:kbd:`TAB` (``S-TAB``)

     Se sitúa en el hipervínculo anterior (``backward-button``).

| ``mouse-1``
| ``mouse-2``

    Sigue un hipervínculo en el que se hace clic.

| :kbd:`n` (``n``)
| :kbd:`p` (``p``)

    Avanza y retrocede entre las páginas del búfer de ayuda.

:kbd:`Ctrl`-:kbd:`c` :kbd:`Ctrl`-:kbd:`c` (``C-c C-c``)

     Muestra toda la documentación sobre el símbolo en el punto (``help-follow-symbol``).

| :kbd:`Ctrl`-:kbd:`c` :kbd:`Ctrl`-:kbd:`f` (``C-c C-f``)
| :kbd:`r` (``r``)

    Avanza en el historial de comandos de ayuda (``help-go-forward``).

| :kbd:`Ctrl`-:kbd:`c` :kbd:`Ctrl`-:kbd:`b` (``C-c C-b``)
| :kbd:`l` (``l``)

    Retrocede en el historial de comandos de ayuda (``help-go-back``).

:kbd:`s` (``s``)

     Consulta la fuente del tema de ayuda actual (si existe) (``help-view-source``).

:kbd:`i` (``i``)

    Busca el tema actual en los manuale(s) (``help-goto-info``).

:kbd:`I` (``I``)

     Busca el tema actual en el manual de Emacs Lisp (``help-goto-lispref-info``).

:kbd:`c` (``c``)

     Personaliza la variable o la cara (``help-customize``).

Cuando un nombre de función, de variable o de cara (véase :ref:`15.8`) aparece en la documentación en el búfer de ayuda, normalmente es un *hipervínculo* subrayado. Para ver la documentación asociada, mueva el punto allí y escriba :kbd:`RETURN` (``RET``, ``help-follow``), o haga clic en el hipervínculo con ``ratón-1`` o ``ratón-2``. Al hacerlo, se reemplaza el contenido del búfer de ayuda. Para volver sobre sus pasos, escriba ``C-c C-b`` o ``l`` (``help-go-back``). Mientras vuelve sobre sus pasos, puede avanzar utilizando ``C-c C-f`` o ``r`` (``help-go-forward``).

Para desplazarse entre hipervínculos en un búfer de ayuda, use :kbd:`TAB` (``TAB``, ``forward-button``) para avanzar al siguiente hipervínculo y :kbd:`Shift`-:kbd:`TAB` (````S-TAB``, ``backward-button``) para retroceder al hipervínculo anterior. Estos comandos actúan cíclicamente; por ejemplo, si se escribe :kbd:`TAB` en el último hipervínculo, se retrocede al primer hipervínculo.

Por defecto, muchos de los enlaces del búfer de ayuda se muestran rodeados de comillas. Si la opción de Usuario ``help-clean-buttons`` no es nula, estos caracteres entrecomillados se eliminan del búfer.

Los búfers de ayuda producidos por algunos comandos de ayuda (como ``C-h b``, que muestra una larga lista de combinaciones de teclas) se dividen en páginas mediante el carácter ``^L``. En estos búferes, el comando :kbd:`n` (``n``, ``help-goto-next-page``) lo llevará al siguiente inicio de página, y el comando :kbd:`p` (``p``, ``help-goto-previous-page``) lo llevará al anterior inicio de página. De esta forma podrá navegar rápidamente entre los distintos tipos de documentación de un búfer de ayuda.

Un búfer de ayuda también puede contener hipervínculos a manuales de información, definiciones de código fuente y URL (páginas web). Los dos primeros se abren en Emacs, y el tercero usando un navegador web a través del comando ``browse-url`` (ver :ref:`47.3`).

Para ver toda la documentación sobre cualquier símbolo en el texto, mueva el puntero sobre el símbolo y escriba :kbd:`Ctrl`-:kbd:`c` :kbd:`Ctrl`-:kbd:`c` (``C-c C-c``, ``help-follow-symbol``). Esto muestra la documentación para todos los significados del símbolo-como una variable, como una función, y/o como una cara.

.. _11.6:

11.6 Búsqueda de Paquetes por Palabras Clave
--------------------------------------------

La mayoría de las características opcionales de Emacs están agrupadas en *paquetes*. Emacs contiene varios cientos de paquetes incorporados, y pueden instalarse más a través de la red (ver :ref:`49`).

Para hacer más fácil encontrar paquetes relacionados con un tema, la mayoría de los paquetes están asociados con una o más palabras *clave basadas* en lo que hacen. Teclee :kbd:`Ctrl`-:kbd:`h` :kbd:`p` (``C-h p``,  ``finder-by-keyword``) para obtener una lista de palabras clave de paquetes, junto con una descripción de lo que significan las palabras clave. Para ver una lista de paquetes para una palabra clave dada, escriba :kbd:`RETURN` (``RET``) en esa línea; esto muestra la lista de paquetes en un búfer de Menú de Paquetes (vea :ref:`49.1`).

:kbd:`Ctrl`-:kbd:`h` :kbd:`P` (``C-h P``, ``describe-package``) pide el nombre de un paquete (ver :ref:`49`), y muestra un búfer de ayuda describiendo los atributos del paquete y las características que implementa. El búfer lista las palabras clave relacionadas con el paquete en forma de botones. Pulse sobre un botón con el ``ratón-1`` o ``ratón-2`` para ver la lista de otros paquetes relacionados con esa palabra clave.

11.7 Ayuda para el Soporte Lingüístico Internacional
----------------------------------------------------

Para obtener información sobre un entorno de idioma específico (consulte :ref:`23.2`), escriba :kbd:`Ctrl`-:kbd:`h` :kbd:`L`  (``C-h L``, ``describe-language-environment``). Aparecerá un búfer de ayuda en el que se describen los idiomas admitidos por el entorno de idioma y se enumeran los conjuntos de caracteres asociados, los sistemas de codificación y los métodos de entrada, así como algunos ejemplos de texto para ese entorno de idioma.

El comando :kbd:`Ctrl`-:kbd:`h` :kbd:`h` (``C-h h``, ``view-hello-file``) muestra el archivo ``etc/HELLO``, que muestra varios juegos de caracteres mostrando cómo decir "hola" en muchos idiomas.

El comando :kbd:`Ctrl`-:kbd:`h` :kbd:`I` (``C-h I``, ``describe-input-method``) describe un método de entrada, ya sea un método de entrada especificado, o por defecto el método de entrada actualmente en uso. Véase :ref:`23.3`.

El comando :kbd:`Ctrl`-:kbd:`h` :kbd:`C` (``C-h C``, ``described-coding-system``) describe sistemas de codificación, ya sea un sistema de codificación especificado o los que se utilizan actualmente. Véase :ref:`23.5`.

.. _11.8:

11.8 Otros comandos de Ayuda
----------------------------

:kbd:`Ctrl`-:kbd:`h` :kbd:`i` (``C-h i``, ``info``) ejecuta el programa Info, que explora archivos de documentación estructurados. :kbd:`Ctrl`-:kbd:`h` :kbd:`4` :kbd:`i` (``C-h 4 i``, ``info-other-window``) hace lo mismo, pero muestra el buffer de Info en otra ventana. El manual completo de Emacs está disponible en Info, junto con muchos otros manuales del sistema GNU. Escriba :kbd:`h` (``h``) después de entrar en Info para ejecutar un tutorial sobre el uso de Info.

Con un argumento numérico *n*, ``C-h i`` selecciona el buffer de Info ``*info*<n>``. Esto es útil si desea navegar por varios manuales Info simultáneamente. Si se especifica sólo :kbd:`Ctrl`-:kbd:`u` (``C-u`` como) argumento prefijo, ``C-h i`` solicita el nombre de un archivo de documentación, de modo que se puede navegar por un archivo que no tenga una entrada en el menú Info de nivel superior.

Los comandos de ayuda :kbd:`Ctrl`-:kbd:`h` :kbd:`F`  *función* :kbd:`RETURN` (``C-h F`` *función* :kbd:`RET`) y :kbd:`Ctrl`-:kbd:`h` :kbd:`K` *tecla* (``C-h K`` *tecla*), descritos anteriormente, entran en Info y van directamente a la documentación de la *función* o *tecla*.

Al editar un programa, si dispone de una versión Info del manual del lenguaje de programación, puede usar :kbd:`Ctrl`-:kbd:`h` :kbd:`S` (``C-h S``, ``info-lookup-symbol``) para encontrar una entrada para un símbolo (palabra clave, función o variable) en el manual adecuado. Los detalles del funcionamiento de este comando dependen del modo principal.

Si ocurre algo inesperado y no está seguro de lo que ha tecleado, utilice :kbd:`Ctrl`-:kbd:`h` :kbd:`l` (``C-h l``, ``view-lossage``). ``C-h l`` muestra sus últimas pulsaciones de teclas y los comandos que invocaron. Por defecto, Emacs almacena las últimas 300 pulsaciones; si lo desea, puede cambiar este número con el comando ``lossage-size``. Si ve comandos con los que no está familiarizado, puede usar ``C-h k`` o ``C-h f`` para averiguar qué hacen.

Para revisar los mensajes recientes del área de eco, utilice :kbd:`Ctrl`-:kbd:`h` :kbd:`e` (``C-h e``, ``view-echo-area-messages``). Esto muestra el buffer ``*Messages*``, donde se guardan esos mensajes.

Cada modo principal de Emacs normalmente redefine algunas teclas y realiza otros cambios en el funcionamiento de la edición. :kbd:`Ctrl`-:kbd:`h` :kbd:`m` (``C-h m``, ``describe-mode``) muestra documentación sobre el modo principal actual, que normalmente describe los comandos y características que cambian en este modo, y también sus atajos de teclado.

:kbd:`Ctrl`-:kbd:`h` :kbd:`b` (``C-h b``, ``describe-bindings``) y :kbd:`Ctrl`-:kbd:`h` :kbd:`s` (``C-h s``, ``describe-syntax``) muestran otra información sobre el entorno actual dentro de Emacs. ``C-h b`` muestra una lista de todas las combinaciones de teclas ahora en efecto: primero las combinaciones locales de los modos menores actuales, luego las combinaciones locales definidas por el modo mayor actual, y finalmente las combinaciones globales (ver :ref:`50.3`). ``C-h s`` muestra el contenido de la tabla de sintaxis, con explicaciones de la sintaxis de cada carácter (**véase Tablas de Sintaxis** en el Manual de Referencia de Emacs Lisp).

Puede obtener una lista de subcomandos para una tecla prefijo en particular tecleando  :kbd:`Ctrl`-:kbd:`h` (``C-h``), :kbd:`?` (``?``), o :kbd:`F1` (``F1``, ``describe-prefix-bindings``) después de la tecla prefijo. (Hay algunas teclas de prefijo para las que no funcionan todas estas teclas, las que proporcionan sus propias combinaciones para esa tecla. Una de estas teclas de prefijo es :kbd:`ESC` (``ESC``), porque :kbd:`ESC` :kbd:`Ctrl`-:kbd:`h` (``ESC C-h``) y :kbd:`ESC` :kbd:`?` (``ESC ?``) son en realidad :kbd:`Ctrl`-:kbd:`Alt`-:kbd:`h` (``C-M-h``, ``mark-defun``) y :kbd:`Alt`-:kbd:`?` (``M-?``, ``xref-find-references``), respectivamente. Sin embargo, :kbd:`ESC` :kbd:`F1` (``ESC F1``) funciona bien).

Finalmente, ``M-x describe-keymap``  pide el nombre de un mapa de teclado, con finalización (o completado), y muestra un listado de todas las combinaciones de teclas en ese mapa de teclado.

.. _11.9:

11.9 Archivos de Ayuda
----------------------

Aparte de la documentación incorporada y los manuales, Emacs contiene otros archivos que describen temas como condiciones de copia, notas de la versión, instrucciones para depurar e informar de errores, etcétera. Puede utilizar los siguientes comandos para ver estos archivos. Aparte de :kbd:`Ctrl`-:kbd:`h` :kbd:`g` (``C-h g``), todos tienen la forma ``C-h`` *char*.

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`Ctrl`-:kbd:`c` (``C-h C-c``)

     Muestra las normas según las cuales puedes copiar y redistribuir Emacs (``describe-copying``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`d` (``C-h C-d``)

     Muestra la ayuda para depurar Emacs (``view-emacs-debbuginge``).
     
:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`e` (``C-h C-e``)

     Muestra información sobre dónde obtener paquetes externos (``view-external-packages``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`f` (``C-h C-f``)

     Muestra la lista de preguntas más frecuentes de Emacs (``view-emacs-FAQ``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`g` (``C-h g``)

     Visita la página con información sobre el Proyecto GNU (``describe-gnu-proyect``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`m` (``C-h C-m``)

     Muestra información sobre cómo solicitar copias impresas de los manuales de Emacs (``view-order-manuals``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`n` (``C-h C-n``)

     Muestra las novedades, que enumeran las nuevas características de esta versión de Emacs (``view-emacs-news``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`o` (``C-h C-o``)

     Muestra cómo solicitar o descargar la última versión de Emacs y otros programas de GNU (``describe-distribution``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`p` (``C-h C-p``)

     Muestra la lista de problemas conocidos de Emacs, a veces con sugerencias para solucionarlos (``view-emacs-problems``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`t` (``C-h C-t``)

     Muestra la lista de tareas pendientes de Emacs (``view-emacs-tools``).

:kbd:`Ctrl`-:kbd:`h` :kbd:`Ctrl`-:kbd:`w` (``C-h C-w``)

     Muestra todos los detalles sobre la ausencia total de garantía para GNU Emacs (``describe-no-warranty``).

.. _11.10:

11.10 Ayuda sobre Texto Activo y Tooltips
-----------------------------------------

En Emacs, los tramos de *texto activo* (texto que hace algo especial en respuesta a clics del ratón o ``RET``) a menudo tienen texto de ayuda asociado. Esto incluye hipervínculos en los buffers de Emacs, así como partes de la línea de modo. En las pantallas gráficas, así como en algunos terminales de texto que soportan el seguimiento del ratón, al mover el ratón sobre el texto activo se muestra el texto de ayuda en forma de *Sugerencia sobre Herramientas* (tooltip). Véase :ref:`22.19`.

En terminales que no soportan el seguimiento del ratón, puede mostrar el texto de ayuda para el texto activo del búfer en el punto tecleando  (``C-h .``, ``display-local-help``). Esto muestra el texto de ayuda en el área de eco. Para mostrar el texto de ayuda automáticamente siempre que esté disponible en el punto, establezca la variable ``help-at-pt-display-when-idle`` a ``t``.

